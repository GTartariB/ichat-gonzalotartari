package cc3002.ichat.core;

import java.net.*;
import java.util.ArrayList;
import java.util.Observable;
import java.io.*;

public class ChatClient extends Observable implements Runnable {
	
	private String user;
	private DataInputStream input;
	private DataOutputStream output;
	private Thread listener;


	public ChatClient (String user,InputStream i, OutputStream o) {
		this.user=user;
		this.input = new DataInputStream (new BufferedInputStream (i));
		this.output = new DataOutputStream (new BufferedOutputStream (o));
		listener = new Thread (this);
		listener.start ();
		
	}
	
	public String username(){
		return user;
	}
	


	public void run () {
		try {
			sendMessage(user);
			while (true) { 
				setChanged();
				notifyObservers(input.readUTF());}
		} catch (IOException ex) { 
			ex.printStackTrace ();
		} finally {
			listener = null;
			try {output.close (); } 
			catch (IOException ex) {ex.printStackTrace ();}
		}
	}
	
	public void sendMessage(String message){
		try {
			output.writeUTF(message);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void closeSession(){
		if (listener != null)
	        listener.stop ();
	}

	public static void main (String args[]) throws IOException {
		String host="localhost";
		int port=2030;
		if (args.length >1){
			host=args[0];
			port=Integer.parseInt(args[1]);
		}

		Socket socket = new Socket (host, port);
		ArrayList<String> features1 = null;
		new ChatClient ("juampi",socket.getInputStream (), socket.getOutputStream ());
	}
}