package cc3002.ichat.client;

import java.awt.BorderLayout;

import javax.swing.*;

public class UIItem extends JPanel{

	private JLabel username;
	private JComponent component;
	public UIItem(String username,JComponent component){
		this.username=new JLabel(username);
		this.component=component;
		setLayout(new BorderLayout());
		add(this.username,BorderLayout.WEST);
		add(this.component,BorderLayout.CENTER);
		//setBackground(Color.WHITE);
		setOpaque(false);
	}
}
