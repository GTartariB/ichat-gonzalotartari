package cc3002.ichat.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTextArea;
import ClientFeatures.ClientFeatures;
import cc3002.ichat.core.*;
import cc3002.ichat.features.*;


public class UIChatGroup extends UIFrame implements Observer{
	private ChatClient client;
	private static ClientFeatures Features;
	
	public UIChatGroup(ChatClient client){
		super(client.username());
		client.addObserver(this);
		this.client=client;	
	}
	/**
	 * Este metodo se llama cada ves que alguien manda un mensaje al server
	 * @param o es un objeto ChatClient que recibio el mensaje del server
	 * @param arg es el mensaje en si
	 * 
	 * arg es de tipo String con el siguiente formato 
	 * username - message
	 */
	@Override
	public void update(Observable o, Object arg) {
		String text=(String) arg;
		int index=text.indexOf("-");
		String username= text.substring(0,index+1);
		String message=text.substring(index+1,text.length());
		//agregando el mensaje a la ventana
		Message mesg=new Sentence(message);
		if(message!=null && !message.contains("has joined.")){
			addHtmlMessage(username,Features.decorateMessages((String) arg));
		}
		else addHtmlMessage(username, message);
		//actualizando la ventana
		updateUI();
	}
	/**
	 * Es metodo se invoka cuando alguien presiona el boton enviar
	 * @text el el objecto que tiene dentro el texto escrito por el usuario
	 */
	@Override
	public void send(JTextArea text) {
		String decoratedMessage=Features.decorateSendMessages(text.getText());
		text.setText(decoratedMessage);
		client.sendMessage(decoratedMessage);
		super.send(text);
	}
	/**
	 * Este metodo se llama cuando alguien cierra la ventana
	 */
	@Override
	public void close() {
		// cerrando la sesion con el server
		client.closeSession();
		super.close();
	}
	
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		ClientFeatures features=new ClientFeatures();
		for(int i=3;i<args.length;i++){
			features.add(args[i]);
		}
		Features=features;
		String host=args[0];
		int port=Integer.parseInt(args[1]);
		String username=args[2];
		Socket socket = new Socket (host, port);
		ChatClient  client=new ChatClient (username,socket.getInputStream (), socket.getOutputStream ());
		UIChatGroup ui=new UIChatGroup(client);
		ui.setVisible(true);
	}
}
