package cc3002.ichat.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ClientFeatures.ClientFeatures;

import cc3002.ichat.features.*;


public class featuresTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBadWordFilter() {
		Message text=(new Sentence("hola wn como estas?"));
		text=new BadWordFilter((text));
		text.decorate();
		assertEquals(text.text(),"hola #@%@& como estas?");
	}

	
	@Test
	public void testEasyPic() {
		Message text=(new Sentence("hola $auxiliar.png$ como estas?"));
		text=new EasyPic((text));
		text.decorate();
		assertEquals(text.text(),"hola <img src=\"file:images/auxiliar.png\"width=50 height=50/> como estas?");
	}
	
	@Test
	public void testMiniSpellCheckerSimple() {
		Message text=(new Sentence("Estoy bn grax"));
		text=new MiniSpellChecker((text));
		text.decorate();
		assertEquals(text.text(),"Estoy <u><font color=\"RED\">bn</font></u> <u><font color=\"RED\">grax</font></u>");
	}
	
	@Test
	public void testMiniSpellCheckerConParentesis() {
		Message text=(new Sentence("hola <img src=\"file:images/auxiliar.png\"width=50 height=50/> cmo estas"));
		text=new MiniSpellChecker((text));
		text.decorate();
		assertEquals(text.text(),"hola <img src=\"file:images/auxiliar.png\"width=50 height=50/> <u><font color=\"RED\">cmo</font></u> estas");
	}
	
	@Test
	public void testHashtagAndLabels() {
		Message text=(new Sentence("hola @auxiliar como estas? #saludos"));
		text=new HashtagAndLabels((text));
		text.decorate();
		assertEquals(text.text(),"hola <em>@auxiliar</em> como estas? <em>#saludos</em>");
	}
	
	@Test
	public void testSourceCode() {
		Message text=(new Sentence("code tamagotchi.doSport();"));
		text=new SourceCode((text));
		text.decorate();
		assertEquals(text.text(),"<code> tamagotchi.doSport(); </code>");
	}
	
	@Test
	public void testLog() {
		Message text=(new Sentence("Gonzalo - Hola mundo."));
		text=new log((text));
		text.decorate();
		assertEquals(text.text().substring(0,24),"Gonzalo , Hola mundo. , ");
	}
	
	@Test
	public void testClientFeaturesAll() {
		ClientFeatures listaDeFeatures=new ClientFeatures();
		listaDeFeatures.add("-all");
		assertTrue(listaDeFeatures.contains("--bwf"));
		assertTrue(listaDeFeatures.contains("--ep"));
		assertTrue(listaDeFeatures.contains("--hl"));
		assertTrue(listaDeFeatures.contains("--log"));
		assertTrue(listaDeFeatures.contains("--msc"));
		assertTrue(listaDeFeatures.contains("--src"));
	}
	
	@Test
	public void testClientFeaturesDefault() {
		ClientFeatures listaDeFeatures=new ClientFeatures();
		listaDeFeatures.add("-default");
		assertTrue(listaDeFeatures.contains("--hl"));
		assertTrue(listaDeFeatures.contains("--ep"));
		assertTrue(listaDeFeatures.contains("--bwf"));
		assertTrue(listaDeFeatures.contains("--log"));
	}
	
	@Test
	public void testDecorateSendMessages() {
		ClientFeatures listaDeFeatures=new ClientFeatures();
		listaDeFeatures.add("-all");
		assertEquals("<code> <img src=\"file:images/auxiliar.png\"width=50 height=50/> </code>"
				,listaDeFeatures.decorateSendMessages("code $auxiliar.png$"));
	}
	
	@Test
	public void testDecorateMessages() {
		ClientFeatures listaDeFeatures=new ClientFeatures();
		listaDeFeatures.add("-all");
		assertEquals("<u><font color=\"RED\"></font></u> hola <em>#@%@&</em> como <u><font color=\"RED\">estas?</font></u>"
				,listaDeFeatures.decorateMessages("usuario - hola wn como estas?"));
	}
}
