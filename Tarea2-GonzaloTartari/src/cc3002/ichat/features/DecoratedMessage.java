package cc3002.ichat.features;


/**
 * The <code>DecoratedMessage</code> class define un programa que extiende a <code>Message</code>.
 * 
 * <p>
 * 
 * 
 * Esta clase corresponde a un mensaje original <code>Sentence</code> modificado con
 * <code>BadWordFilter</code>, <code>EasyPic</code> y/o <code>MiniSpellChecker</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class DecoratedMessage extends Message{

    /**
     * decoratedMessage guarda un mensaje del tipo <code>Message</code>.
         * @serial
     */
    protected Message decoratedMessage;
    
	/**
	 * Constructor e inicializa <code>DecoratedMessage</code> con
	     * un objeto del tipo Message.
	     *
	 */
    public DecoratedMessage(Message dc){
        decoratedMessage = dc;    
    }
    
    /**
     * Decora el mensaje decoratedMessage con el metodo decorate() dependiendo del
     * tipo de objeto de decoratedMessage(<code>BadWordFilter</code>, <code>EasyPic</code> 
     * o <code>MiniSpellChecker</code>).
     * 
     */
    public void decorate(){
        decoratedMessage.decorate();
    }
    
    /**
     * Retorna <code>String</code> que contiene el mensaje contenido en el objeto del
     * tipo <code>Message</code>.
         * @return el string del mensaje.
     */
	@Override
	public String text() {
		return decoratedMessage.text();
	}
	
    /**
     * Reemplaza la variable decoratedMessage por el string t.
         * @param t es el texto a guardar en el objeto.
     */
	@Override
	public void setText(String t) {
		decoratedMessage.setText(t);		
	}
}
