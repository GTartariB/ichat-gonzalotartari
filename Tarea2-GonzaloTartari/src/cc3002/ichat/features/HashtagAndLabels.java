package cc3002.ichat.features;


/**
 * The <code>HashtagAndLabels</code> class define un programa que al encontrar una palabra que
 * comienza con # o @ en un string le cambia el formato a uno que permite visualizar la palabra
 * con enfasis.
 * 
 * <p>
 * 
 * 
 * Esta clase extiende la clase <code>DecoratedMessage</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class HashtagAndLabels extends DecoratedMessage{

	/**
	 * Constructor e inicializa <code>HashtagAndLabels</code> con
	     * un objeto del tipo Message.
	     *
	 */
	public HashtagAndLabels(Message dc) {
		super(dc);
	}

	  /**
     * Busca las palabras que comienzan con # o @ y cambia su formato para que sean visibles con
     * enfasis en html. Esto lo lleva a cabo separando el string del mensaje en strings de palabras 
     * y revisando si hay un # o @ al comienzo de esta.
     * 
     */
    public void decorate(){
    	String[] a= decoratedMessage.text().split(" ");
    	for(int i=0;i<a.length;i++){
    		if(a[i].length()>0){
	    		if(a[i].charAt(0)=='#' || a[i].charAt(0)=='@'){
	    			a[i]="<em>"+a[i]+"</em>";
	    		}
	    	}
    	}
    	decoratedMessage.setText(a[0]);
    	for(int j=1;j<a.length;j++){
    		decoratedMessage.setText(decoratedMessage.text()+" "+a[j]);
    	}
        decoratedMessage.decorate();
    }
    
    
}
