package cc3002.ichat.features;

import java.util.Arrays;
import java.util.List;



/**
 * The <code>BadWordFilter</code> class define un programa que permite filtrar un conjunto de  
 * palabras reemplazandolas por "#@%@&".
 * 
 * <p>
 * 
 * 
 * Esta clase extiende la clase <code>DecoratedMessage</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */

public class BadWordFilter extends DecoratedMessage{

    /**
     * stringList guarda las palabras a filtrar por <code>BadWordFilter</code>.
         * @serial
     */
	private List<String> stringList = Arrays.asList(new String[]{"diantres", "recorcholis", "ctm" , "wn" , "qlo"});
	
	
	/**
	 * Constructor e inicializa <code>BadWordFilter</code> con
	     * un objeto del tipo Message.
	     *
	 */
	public BadWordFilter(Message dc) {
		super(dc);
	}

    /**
     * Compara las palabras del mensaje que esten en la lista stringList y las reemplaza
     * por "#@%@&". Esto lo lleva a cabo separando el string del mensaje en strings de palabras 
     * y comparandolas con las de la lista.
     * 
     */
    public void decorate(){
    	String[] a= decoratedMessage.text().split(" ");
    	for(int i=0;i<a.length;i++){
    		if(stringList.contains(a[i].toLowerCase())){
    			a[i]="#@%@&";
    		}
    	}
    	decoratedMessage.setText(a[0]);
    	for(int j=1;j<a.length;j++){
    		decoratedMessage.setText(decoratedMessage.text()+" "+a[j]);
    	}
        decoratedMessage.decorate();
    }
}
