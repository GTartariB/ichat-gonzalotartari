package cc3002.ichat.features;

/**
 * The <code>log</code> class define un programa que toma un texto y divide el usuario
 * con el mensaje(separados por un "-") luego imprime esto separado por una coma y agrega
 * tambien la fecha en la que se envio.
 * 
 * <p>
 * 
 * 
 * Esta clase extiende la clase <code>DecoratedMessage</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class log extends DecoratedMessage{

	/**
	 * Constructor e inicializa <code>BadWordFilter</code> con
	     * un objeto del tipo Message.
	     *
	 */
	public log(Message dc) {
		super(dc);
	}

    /**
     * Toma un texto y divide el usuario con el mensaje(separados por un "-") luego imprime 
     * esto separado por una coma y agrega tambien la fecha en la que se envio.
     * 
     */
	public void decorate(){
		int index=decoratedMessage.text().indexOf("-");
		String username= decoratedMessage.text().substring(0,index);
		String message=decoratedMessage.text().substring(index+1,decoratedMessage.text().length());
		decoratedMessage.setText(username+","+message+" , "+new java.util.Date().toString());
		decoratedMessage.decorate();
	}
	
}
