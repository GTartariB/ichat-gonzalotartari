package cc3002.ichat.features;


import java.util.Arrays;
import java.util.List;



/**
 * The <code>MiniSpellChecker</code> class define un programa que subraya y cambia a 
 * color rojo en el formato html a todas las palabras que no se encuentren en la lista
 * stringList.
 * 
 * <p>
 * 
 * 
 * Esta clase extiende la clase <code>DecoratedMessage</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class MiniSpellChecker extends DecoratedMessage{
	
    /**
     * stringList guarda las palabras a comparar por <code>MiniSpellChecker</code>.
         * @serial
     */
	private List<String> stringList = Arrays.asList(new String[]{"hola", "como", "estas","#@%@&","bien",
			"gracias","la","de","yo","tu","el","nosotros","ustedes","ellos","nada","todo","mal","chao",
			"casa","perro","gato","auto","avion","barco","bus","adios","buenos","dia","dias","tardes",
			"tarde","ma�ana","ayer","pasado","futuro","computador","monitor","television","radio", "estoy",
			"internet","tarea","control","examen","promedio","curso","clase","sala","programa","blanco",
			"azul","rojo"});
	
	
	/**
	 * Constructor e inicializa <code>MiniSpellChecker</code> con
	     * un objeto del tipo Message.
	     *
	 */
    public MiniSpellChecker(Message dc){
        super(dc);
    }
    
    /**
     * Compara las palabras del mensaje que esten en la lista stringList y si no estan las subraya
     * y le cambia el color a rojo en html. Esto lo lleva a cabo separando el string del mensaje en 
     * strings de palabras y comparandolas con las de la lista.
     * No modifica strings que esten entre caracteres > y < para no modificar texto utilizado
     * para html.
     * 
     */
    public void decorate(){
    	int NumeroParentesis=0;
    	String[] a= decoratedMessage.text().split(" ");
    	for(int i=0;i<a.length;i++){
    		if(!stringList.contains(a[i].toLowerCase())){
    			String h=a[i];
    			boolean c=false;
    			if(a[i].contains("<")) {
    				int num=h.replaceAll("<", "<<").length()-a[i].length();
    				NumeroParentesis=NumeroParentesis+num;
    				}
    			if(a[i].contains(">")) c=true;
    			if(NumeroParentesis<1) {a[i]="<u><font color="+"\"RED\""+">"+a[i]+"</font></u>";}
    			if(c) {
    				int num2=h.replaceAll(">", ">>").length()-a[i].length();
    				NumeroParentesis=NumeroParentesis-num2;}
    		}
    	}
    	decoratedMessage.setText(a[0]);
    	if(a.length>0){
	    	for(int j=1;j<a.length;j++){
	    		decoratedMessage.setText(decoratedMessage.text()+" "+a[j]);
	    	}
    	}
        decoratedMessage.decorate();
    }

}
