package cc3002.ichat.features;


/**
 * The <code>Message</code> class define una clase abstracta que sera extendida por 
 * <code>Sentence</code>(mensaje original) o <code>DecoratedMessage</code>(mensaje decorado).
 * 
 * <p>
 * 
 * 
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public abstract class Message {
	
    /**
     * Decora el mensaje Message con el metodo decorate().
     * 
     */
    public abstract void decorate();
    
    /**
     * Reemplaza el mensaje guardado por el objeto por el string t.
         * @param t es el texto a guardar en el objeto.
     */
    public abstract void setText(String t);
    
    /**
     * Retorna <code>String</code> que contiene el mensaje contenido en el objeto del
     * tipo <code>Message</code>.
         * @return el string del mensaje.
     */
    public abstract String text();

}
