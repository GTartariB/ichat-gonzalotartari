package cc3002.ichat.features;

/**
 * The <code>SourceCode</code> class define un programa que al encontrar la palabra code
 * al comienzo del mensaje cambia el formato del mensaje para que se muestre como c�digo 
 * fuente.
 * 
 * <p>
 * 
 * 
 * Esta clase extiende la clase <code>DecoratedMessage</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class SourceCode extends DecoratedMessage{
	
	/**
	 * Constructor e inicializa <code>BadWordFilter</code> con
	     * un objeto del tipo Message.
	     *
	 */
	public SourceCode(Message dc) {
		super(dc);
	}
	
	 /**
     * Al encontrar la palabra code al comienzo del mensaje cambia el formato del mensaje 
     * para que se muestre como c�digo fuente. Si no esta la palabra code al comienzo no se
     * realiza ninguna acci�n adicional.
     * 
     */
    public void decorate(){
    	String[] a= decoratedMessage.text().split(" ");
		if(a[0].toLowerCase().equals("code")){
			a[0]="<code>";	
		
    	decoratedMessage.setText(a[0]);
    	for(int j=1;j<a.length;j++){
    		decoratedMessage.setText(decoratedMessage.text()+" "+a[j]);
    	}
    	decoratedMessage.setText(decoratedMessage.text()+" "+"</code>");}
		else decoratedMessage.decorate();
    }
}
