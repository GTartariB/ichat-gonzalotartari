package cc3002.ichat.features;


/**
 * The <code>EasyPic</code> class define un programa que al encontrar una palabra encerrada
 * por $ en un string le cambia el formato a uno que permite visualizar imagenes en html.
 * 
 * <p>
 * 
 * 
 * Esta clase extiende la clase <code>DecoratedMessage</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class EasyPic extends DecoratedMessage{
	
	/**
	 * Constructor e inicializa <code>BadWordFilter</code> con
	     * un objeto del tipo Message.
	     *
	 */
	public EasyPic(Message dc) {
		super(dc);
	}
	
    /**
     * Busca las palabras encerradas por $ y cambia su formato para que sean visibles como
     * imagenes en html. Esto lo lleva a cabo separando el string del mensaje en strings de palabras 
     * y revisando si hay un $ al comienzo y al final de esta.
     * 
     */
    public void decorate(){
    	String[] a= decoratedMessage.text().split(" ");
    	for(int i=0;i<a.length;i++){
    		if(a[i].length()>0){
    		if(a[i].charAt(0)=='$' && a[i].charAt(a[i].length()-1)=='$'){
    			a[i]="<img src=\"file:images/"+a[i].substring(1, a[i].length()-1)+"\"width=50 height=50/>";
    		}}
    	}
    	decoratedMessage.setText(a[0]);
    	for(int j=1;j<a.length;j++){
    		decoratedMessage.setText(decoratedMessage.text()+" "+a[j]);
    	}
        decoratedMessage.decorate();
    }

}
