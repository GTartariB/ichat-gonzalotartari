package cc3002.ichat.features;


/**
 * The <code>Sentence</code> class define un programa que extiende a <code>Message</code>.
 * 
 * <p>
 * 
 * 
 * Esta clase corresponde a un mensaje original <code>Sentence</code> que no ha sido modificado con
 * <code>BadWordFilter</code>, <code>EasyPic</code> y/o <code>MiniSpellChecker</code>.
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class Sentence extends Message{

	
    /**
     * text guarda un mensaje del tipo <code>String</code>.
         * @serial
     */
    private String text;
    
	/**
	 * Constructor e inicializa <code>Sentence</code> con
	     * un mensaje guardado en un string.
	     *
	 */
    public Sentence(String text){
        this.text=text;
    }
    
    
    /**
     * En este caso no hace nada ya que es un mensaje no decorado.
     * 
     */
    public void decorate(){
    }

    /**
     * Retorna <code>String</code> que contiene el mensaje contenido en el objeto del
     * tipo <code>Sentence</code>.
         * @return el string del mensaje.
     */
	@Override
	public String text() {
		return text;
	}

    /**
     * Reemplaza la variable text por el string t.
         * @param t es el texto a guardar en el objeto.
     */
	@Override
	public void setText(String t) {
		this.text=t;
		
	}
    

}
