package ClientFeatures;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import cc3002.ichat.features.*;

/**
 * The <code>ClientFeatures</code> class define un programa que permite guardar y aplicar
 * las "features" solicitadas por el cliente.
 * 
 * <p>
 * 
 * 
 *
 * @version  2   
 * @author  Gonzalo Tartari
 */
public class ClientFeatures {
	
    /**
     * FeatureList guarda las features a utilizar por <code>UIChatGroup</code>.
         * @serial
     */
	private ArrayList<String> FeatureList;
	
	/**
	 * Constructor e inicializa <code>ClientFeatures</code>-
	     *
	 */
	public ClientFeatures(){
		FeatureList=new ArrayList<String>();
	}
	
	 /**
     * agrega al array list FeatureList la feature pedida, si �sta es "-all" o "-default"
     * agrega los features correspondientes.
     * 
     */
	public void add(String feature){
		FeatureList.add(feature);
		if(FeatureList.contains("-all")){
			FeatureList.clear();
			FeatureList.add("--bwf");
			FeatureList.add("--msc");
			FeatureList.add("--hl");
			FeatureList.add("--log");
			FeatureList.add("--src");
			FeatureList.add("--ep");
		}
		if(FeatureList.contains("-default")){
			FeatureList.clear();
			FeatureList.add("--hl");
			FeatureList.add("--ep");
			FeatureList.add("--bwf");
			FeatureList.add("--log");
		}
	}
	
	 /**
     * decora el mensaje recibido con los features contenidos el la lista guardada 
     * con los features.
     * 
     */
	public String decorateSendMessages(String mensaje){
		Message mesg=new Sentence(mensaje);
		if(!FeatureList.isEmpty()){
			for(int i=0;i<FeatureList.size();i++){
				if(FeatureList.get(i).equals("--src")){
					mesg= new SourceCode(mesg);
					mesg.decorate();
				}
				if(FeatureList.get(i).equals("--ep")){
					mesg= new EasyPic(mesg);
					mesg.decorate();
				}
			}
		}
		return mesg.text();
	}

	 /**
     * decora el mensaje recibido con los features contenidos el la lista guardada 
     * con los features.
     * 
     */
	public String decorateMessages(String mensaje){
		String text=mensaje;
		int index=text.indexOf("-");
		String message=text.substring(index+1,text.length());
		Message mesg=new Sentence(message);
		if(message!=null && !message.contains("has joined.")){
			if(!FeatureList.isEmpty()){
				for(int i=0;i<FeatureList.size();i++){
					if(FeatureList.get(i).equals("--bwf")){
						mesg= new BadWordFilter(mesg);
						mesg.decorate();
					}
					if(FeatureList.get(i).equals("--msc")){
						mesg= new MiniSpellChecker(mesg);
						mesg.decorate();
					}
					if(FeatureList.get(i).equals("--hl")){
						mesg= new HashtagAndLabels(mesg);
						mesg.decorate();
					}
					if(FeatureList.get(i).equals("--log")){
						Message mesg1= new log(new Sentence(text));
						mesg1.decorate();
						append(new File("C:\\temp\\log.txt"), mesg1.text());
					}
				}
			}
		}
		return mesg.text();
	}
	
	 /**
     * metodo utilizado para imprimir un string en un archivo de texto.
     * 
     */
	  public static void append(File aFile, String content) {
		    try {
		      PrintStream p = new PrintStream(new BufferedOutputStream(new FileOutputStream(aFile, true)));
		      p.println(content);
		      p.close();

		    } catch (Exception e) {
		      e.printStackTrace();
		      System.err.println(aFile);
		    }
		  }
	  
		 /**
	     * metodo para tener acceso a la lista de features.
	     * 
	     */
	  //metodo necesario para testear
	  public boolean contains(String feature){
		  return FeatureList.contains(feature);
	  }
	  

}
